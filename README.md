# Decoding Engagement: The Role of Closeness Cues in Human-Robot Interactions

Welcome to the repository for "Decoding Engagement," a sophisticated engagement detection system tailored for enhancing human-robot interactions. This Python-based module leverages advanced image analysis techniques to interpret closeness cues from video streams, offering insightful scores that reflect the level of engagement.

## Overview

The `Decoding Engagement` system is designed to seamlessly integrate into your projects, providing a robust and intuitive interface for engagement analysis. By analyzing video stream images, it generates quantitative engagement scores, enabling developers and researchers to gauge interaction effectiveness in real-time.

## Getting Started

### Prerequisites

Ensure you have Python installed on your system. This module requires the following libraries: OpenCV, Mediapipe, Numpy, and FER. These dependencies are vital for image processing and engagement score computation.

### Installation

First, clone this repository to your local machine or download the `wtc.py` file directly. Then, install the required packages using the following commands:

```bash
pip install opencv-python mediapipe numpy fer
```

### Quick Example

Here's a quick example to get you started with the `Decoding Engagement` system:

```Python
from wtc import ScoreComputation
import cv2

# Initialize the ScoreComputation object for video stream analysis
wtc_object = ScoreComputation()

# Load an image from your project directory
image = cv2.imread('example.jpg')

# Compute the engagement score
score = wtc_object.getMainScore(image)

# 'score' now contains a value between 0 and 1, indicating engagement level
print(f"Engagement Score: {score}")
```
### Licence
This project is licensed under the MIT License.
