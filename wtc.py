import cv2
import mediapipe as mp
import numpy as np
import math
from fer import FER

class ScoreComputation:


    def getMainScore(self,frame):
        orientation_score, emotion_score, gaze_score, posture_score, proximity_score, score = self.main(frame)
        return score


    def main(self,frame):

        orientation_score = 0
        emotion_score = 0
        gaze_score = 0
        posture_score = 0
        proximity_score = 0
        #volume_score = 0
        score = 0


        # facemesh prediction
        with self.mp_face_mesh.FaceMesh(
                static_image_mode=True, # one image at a time
                max_num_faces=1, # one face only prediction
                refine_landmarks=True, # we want info about the iris and eye position
                min_detection_confidence=0.5) as face_mesh: # default value
                # Read image file with cv2 and convert from BGR to RGB
                face_results = face_mesh.process(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))

        # saving the last 10 face mesh objects
        self.last_facemeshes_list.append(face_results)
        # only store a max number of objects in this list
        if len(self.last_facemeshes_list) > self.last_facemeshes_list_max_number:
            self.last_facemeshes_list.pop(0)

        # ------- pose prediction -------
        with self.mp_pose.Pose(static_image_mode=True,
                    model_complexity=2,
                    enable_segmentation=True) as pose:
            pose_results = pose.process(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))

        # if a body was detected by mediapipe
        if pose_results.pose_landmarks:
            # vector_orientation is the vector for the camera view direction
            vector1 = np.array([0,0])
            vector2 = np.array([0,1])
            vector_orientation = np.subtract(vector2, vector1)
            # shoulder direction vector
            vector3 = np.array([pose_results.pose_landmarks.landmark[12].x,pose_results.pose_landmarks.landmark[12].z]) # right shoulder
            vector4 = np.array([pose_results.pose_landmarks.landmark[11].x,pose_results.pose_landmarks.landmark[11].z]) # left shoulder
            vector_shoulders = np.subtract(vector4, vector3)
            # angel between vector_shoulders and x axis and vector_orientation and x axix substracted
            orientation_shoulders = math.atan2(vector_shoulders[1], vector_shoulders[0]) - math.atan2(vector_orientation[1], vector_orientation[0])
            orientation_shoulders = np.abs(np.degrees(orientation_shoulders)) - 90 # angle is shifted by 90 degrees

            # hip direction vector
            vector5 = np.array([pose_results.pose_landmarks.landmark[23].x,pose_results.pose_landmarks.landmark[23].z]) # left hip
            vector6 = np.array([pose_results.pose_landmarks.landmark[24].x,pose_results.pose_landmarks.landmark[24].z]) # right hip
            vector_hips = np.subtract(vector5, vector6)
            # angel between vector_hips and x axis and vector_orientation and x axix substracted
            orientation_hips = math.atan2(vector_hips[1], vector_hips[0]) - math.atan2(vector_orientation[1], vector_orientation[0])
            orientation_hips = np.abs(np.degrees(orientation_hips)) - 90 # angle is shifted by 90 degrees

            # head direction vector
            vector7 = np.array([pose_results.pose_landmarks.landmark[7].x,pose_results.pose_landmarks.landmark[7].z]) # left ear
            vector8 = np.array([pose_results.pose_landmarks.landmark[8].x,pose_results.pose_landmarks.landmark[8].z]) # right ear
            vector_head = np.subtract(vector7, vector8)
            # angel between vector_head and x axis and vector_orientation and x axix substracted
            orientation_head = math.atan2(vector_head[1], vector_head[0]) - math.atan2(vector_orientation[1], vector_orientation[0])
            orientation_head = np.abs(np.degrees(orientation_head)) - 90 # angle is shifted by 90 degrees

            # ------- fer object for emotion detection -------
            # (computing a new fer object every frame is to expensive)
            # getting fer object if the last one is to old
            if self.frame_counter == self.counter_num_exp_action or not self.last_fer_object:
                emotion_fer_object = self.emotion_detector.detect_emotions(frame)
                self.last_fer_object = emotion_fer_object # using this fer object until the new computation
            else:
                #emotion_fer_object = last_fer_object
                emotion_fer_object = self.emotion_detector.detect_emotions(frame) # we can detect the emotion in every frame in the evaluation

            # ------- get main score -------
            orientation_score, emotion_score, gaze_score, posture_score, proximity_score, score = self.getScore(
                orientation_head, orientation_shoulders, orientation_hips, emotion_fer_object, self.last_facemeshes_list, pose_results)
                
        cv2.imshow('wtc',frame)

        return orientation_score, emotion_score, gaze_score, posture_score, proximity_score, score #, volume_score, score

                
    #### ------------------- ####
    #### Computing the Score ####
    #### ------------------- ####

    # will return the main willingnes-to-communicate score
    def getScore(self, orientation_head, orientation_shoulders, orientation_hips, emotion_fer_object, faceMeshList, pose_landmark_list):

        # weights for subcomponents
        orientation_factor = 0.9
        emotion_factor = 0.9
        gaze_factor = 1
        posture_factor = 0.9
        proximity_factor = 0.7
        #volume_factor = 0.1

        # scores of the single components; every score is between 0 and 1
        orientation_score = self.orientationScore(orientation_head, orientation_shoulders, orientation_hips)
        emotion_score = self.emotionScore(emotion_fer_object)
        gaze_score = self.gazeScore(faceMeshList)
        posture_score = self.postureScore(pose_landmark_list)
        proximity_score = self.proximityScore(faceMeshList)
        #volume_score = self.volumeScore()

        # comute the weighted mean and get the main score
        all_factors = orientation_factor + emotion_factor + gaze_factor + posture_factor + proximity_factor# + volume_factor
        score = (orientation_score*orientation_factor + emotion_score*emotion_factor + gaze_score*gaze_factor + 
                    posture_score*posture_factor + proximity_score*proximity_factor# + volume_score*volume_factor
                    ) / all_factors

        # debug prints
        #print("---")
        #print("orientationS: ", orientation_score)
        #print("    emotionS: ", emotion_score)
        #print("       gazeS: ", gaze_score)
        #print("    postureS: ", posture_score)
        #print("  proximityS: ", proximity_score)
        #print("     volumeS: ", volume_score)
        #print(" final Score: ", score)

        return orientation_score, emotion_score, gaze_score, posture_score, proximity_score, score #, volume_score, score

    # ----------------------------

    # proximity subscore
    def proximityScore(self, faceMeshList):
        # if there is an fecemeshset in the list, use the first one
        for index in range(len(faceMeshList)):
            results = faceMeshList[index]
            if results.multi_face_landmarks:
                # landmark of the left side of the jaw
                left_jaw = np.array([results.multi_face_landmarks[0].landmark[58].x,
                                        results.multi_face_landmarks[0].landmark[58].y,
                                        results.multi_face_landmarks[0].landmark[58].z])
                # landmark of the right side of the jaw
                right_jaw = np.array([results.multi_face_landmarks[0].landmark[288].x,
                                        results.multi_face_landmarks[0].landmark[288].y,
                                        results.multi_face_landmarks[0].landmark[288].z])
                # direction vector of the jaw
                jaw_vector = np.subtract(left_jaw, right_jaw)
                jaw_vector_length = np.linalg.norm(jaw_vector)
                # proximity function
                return self.proximityF(jaw_vector_length*100) # multiplier for working with simpler numbers
        return 0

    # function for the proximity score -> a logarithmic function through the points P0(10/0), P1(12/0.5) and P2(18/1)
    def proximityF(self, x):
        a = 0.5/(math.log(9)-math.log(3)) # approximate 0.455
        b = -9
        if x < 10:
            return 0
        elif x > 18:
            return 1
        else:
            return math.log(x+b)*a

    # ----------------------------

    # orientation subscore
    def orientationScore(self, orientation_head, orientation_shoulders, orientation_hips):
        subscore_head = self.orientationF(orientation_head)
        subscore_shoulders = self.orientationF(orientation_shoulders)
        subscore_hips = self.orientationF(orientation_hips)

        return (4*subscore_head + subscore_shoulders + subscore_hips) / 6


    # function for orientation score -> a logarithmic function through the points P1(5/1) and P2(90/0)
    def orientationF(self, x):
        x = np.abs(x) # we only want positive degrees
        if x <= 5:
            return 1
        elif x >= 90:
            return 0
        else:
            b = 1/(math.log(90)-math.log(5)) # approximate 0.34597
            c = 1 + b*math.log(5) # approximate 1.55682
            return -b * math.log(x) + c

    # ----------------------------

    # emotion subscore
    # emotions: 'angry','disgust','fear','happy','sad','surprise','neutral'
    def emotionScore(self, fer_analysis_object):
        # in case there is no fer_analysis_object return 0
        if len(fer_analysis_object) < 1:
            return 0

        # 1. part
        # get factor of dominant emotion
        # makes up 50 percent of the final emotion score
        dominant_emotion = self.getDominantEmotion(fer_analysis_object)
        if dominant_emotion == "happy" or dominant_emotion == "surprise" or dominant_emotion == "fear":
            part_one_score = 1
        elif dominant_emotion == "sad":
            part_one_score = 0.7
        elif dominant_emotion == "neutral":
            part_one_score = 0.5
        elif dominant_emotion == "disgust":
            part_one_score = 0.2
        else:
            part_one_score = 0

        # 2. part
        # talking every emotion value into account that is high enough
        happy_value = fer_analysis_object[0]['emotions']['happy']
        disgust_value = fer_analysis_object[0]['emotions']['disgust']
        fear_value = fer_analysis_object[0]['emotions']['fear']
        angry_value = fer_analysis_object[0]['emotions']['angry']
        sad_value = fer_analysis_object[0]['emotions']['sad']
        surprise_value = fer_analysis_object[0]['emotions']['surprise']
        neutral_value = fer_analysis_object[0]['emotions']['neutral']

        # summing up all important emotion factors
        counter = 0
        part_two_score = 0
        boundary = 0.4
        if happy_value > boundary:
            counter = counter + 1
            part_two_score = part_two_score + 1
        if surprise_value > boundary:
            counter = counter + 1
            part_two_score = part_two_score + 1
        if fear_value > boundary:
            counter = counter + 1
            part_two_score = part_two_score + 1
        if sad_value > boundary:
            counter = counter + 1
            part_two_score = part_two_score + 0.7
        if neutral_value > boundary:
            counter = counter + 1
            part_two_score = part_two_score + 0.5
        if disgust_value > boundary:
            counter = counter + 1
            part_two_score = part_two_score + 0.2
        if angry_value > boundary:
            counter = counter + 1

        # mean of the important factors if there are important factors
        if counter > 0:
            tmp = part_two_score / counter
        else:
            tmp = part_one_score

        return (part_one_score+tmp)/2

    # method to geht the dominant emotion with the fer_analysis_object as parameter
    # faster than the method, that fer provided
    def getDominantEmotion(self, fer_analysis_object):
        if len(fer_analysis_object) > 0:
            dominant_key = 'neutral'
            dominant_key_value = 0.0
            for key in fer_analysis_object[0]['emotions'].keys(): # index 0 -> dict of the first person
                if fer_analysis_object[0]['emotions'][key] >= dominant_key_value:
                    dominant_key = key
                    dominant_key_value = fer_analysis_object[0]['emotions'][key]

            return dominant_key
        else:
            return 'none'

    # ----------------------------

    # gaze subscore
    # face_landmark_result_list is the list of the last 10 detected face meshs
    def gazeScore(self, face_landmark_result_list):
        
        gaze_scores_list = []

        # checking whether the person is looking at the camera
        for index in range(len(face_landmark_result_list)):

            results = face_landmark_result_list[index]
            if results.multi_face_landmarks:
                iris_left = np.array([results.multi_face_landmarks[0].landmark[468].x,
                                        results.multi_face_landmarks[0].landmark[468].z])
                left_eye_inner_corner = np.array([results.multi_face_landmarks[0].landmark[133].x,
                                        results.multi_face_landmarks[0].landmark[133].z])
                left_eye_outer_corner = np.array([results.multi_face_landmarks[0].landmark[33].x,
                                        results.multi_face_landmarks[0].landmark[33].z])
                left_jaw = np.array([results.multi_face_landmarks[0].landmark[58].x,
                                        results.multi_face_landmarks[0].landmark[58].z])
                right_jaw = np.array([results.multi_face_landmarks[0].landmark[288].x,
                                        results.multi_face_landmarks[0].landmark[288].z])
                
                distance_iris_outer = np.linalg.norm(iris_left-left_eye_outer_corner) # distance iris to outer eye corner
                distance_iris_inner = np.linalg.norm(iris_left-left_eye_inner_corner) # distance iris to inner eye corner

                # linear interpolization with 160 degree of eyesight
                # because of mediapipe depth characteristics holds: iris in the middle of the eye -> straight gazing
                eye_sight = 160
                angle_from_inner_corner = (distance_iris_inner / (distance_iris_inner+distance_iris_outer)) * eye_sight # angle between iris and inner eye corner
                eye_angle = (eye_sight/2)-angle_from_inner_corner # angle from the middle of the eye to the iris

                # jaw direction vector and unit vector of the jaw direction vector
                jaw_vector = np.subtract(left_jaw, right_jaw)
                jaw_vector_u = jaw_vector / np.linalg.norm(jaw_vector)

                # vector for camera sight direction and unit vector of it
                vector1 = np.array([0,0])
                vector2 = np.array([0,-1])
                vector_orientation = np.subtract(vector2, vector1)
                vector_orientation_u = vector_orientation / np.linalg.norm(vector_orientation)

                # head angle in regards to the camera position
                head_angle = np.arccos( np.dot(jaw_vector_u, vector_orientation_u))
                head_angle = 90 - np.degrees(head_angle)

                # gaze angle in regards to the head angle and camera position
                gaze_angle = head_angle + eye_angle #balance each other out for eye contact

                gaze_scores_list.append((np.abs(gaze_angle) < 13)) # 5 (like the literature says) is to small for the given accuracy

            else:
                gaze_scores_list.append(False)

        score = np.count_nonzero(gaze_scores_list) # counts true conditions
        return score/len(gaze_scores_list) # there are max 10 facemesh objects in the queue -> computes the mean

    # ----------------------------

    # posture subscore
    # uses the posture landmarks as input parameters
    def postureScore(self, pose_landmark_list):
        # initial variable for summing up the score through condition checks
        score = 0

        # --- Positive Influence ---
        # -- Bow to camera --
        # for gentle and subtle bows
        # a bigger z coordinate value means the landmark is more in the background
        if pose_landmark_list.pose_landmarks.landmark[12].z < pose_landmark_list.pose_landmarks.landmark[24].z and pose_landmark_list.pose_landmarks.landmark[11].z < pose_landmark_list.pose_landmarks.landmark[23].z:
            score = score + 0.25
        # -- Hands between camera and body --
        if pose_landmark_list.pose_landmarks.landmark[16].z < pose_landmark_list.pose_landmarks.landmark[12].z and pose_landmark_list.pose_landmarks.landmark[15].z < pose_landmark_list.pose_landmarks.landmark[11].z:
            score = score + 0.25
        # -- Right hand between the shoulders --
        if pose_landmark_list.pose_landmarks.landmark[16].x > pose_landmark_list.pose_landmarks.landmark[12].x and pose_landmark_list.pose_landmarks.landmark[16].x < pose_landmark_list.pose_landmarks.landmark[11].x:
            score = score + 0.25
        # -- Left hand between the shoulders --
        if pose_landmark_list.pose_landmarks.landmark[15].x > pose_landmark_list.pose_landmarks.landmark[12].x and pose_landmark_list.pose_landmarks.landmark[15].x < pose_landmark_list.pose_landmarks.landmark[11].x:
            score = score + 0.25
            
        # --- Negative Influence ---
        # -- Crossed arms --
        # left hand is more right than the right hand
        if pose_landmark_list.pose_landmarks.landmark[15].x < pose_landmark_list.pose_landmarks.landmark[16].x:
            score = score - 0.25
        # -- Crossed legs --
        # left foot is more right than the right foot 
        if pose_landmark_list.pose_landmarks.landmark[27].x < pose_landmark_list.pose_landmarks.landmark[28].x:
            score = score - 0.25

        # keep the score within the boundaries before returning it
        if score > 1:
            return 1
        elif score < 0:
            return 0
        else:
            return score
        
    # ----------------------------

    
    def __init__(self):
        # variables for expensive actions
        self.counter_num_exp_action = 30 # every 30 seconds the expensive actions will be computed
        self.frame_counter  = 0

        # emotion detector object 
        self.emotion_detector = FER(mtcnn=True) # will be created one time for all predictions

        # variables to store the last face mesh predictions
        self.last_facemeshes_list = []
        self.last_facemeshes_list_max_number = 10

        # variable that stores the last predicted fer_object
        self.last_fer_object = None # will not be computed every frame

        # mediapipe instances for their corresponding cases
        self.mp_drawing = mp.solutions.drawing_utils
        self.mp_drawing_styles = mp.solutions.drawing_styles
        self.mp_pose = mp.solutions.pose
        self.mp_face_mesh = mp.solutions.face_mesh
